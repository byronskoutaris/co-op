import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Header from './Components/Header/Header';
import SingleJoke from './Components/Jokes/SingleJoke/SingleJoke';
import SearchJoke from './Components/Jokes/SearchJoke/SearchJoke';
import List from './Components/Jokes/List/List';

class App extends React.Component {
	render() {
		return (
      <Router>
        <div className="App">
				<Header />
				<Switch>
					<Router>
						<Route exact path="/" component={SingleJoke} />
						<Route exact path="/search-joke" component={SearchJoke} />
						<Route exact path="/never-ending-list" component={List} />
					</Router>
				</Switch>
        </div>
        </Router>
		);
	}
}

export default App;
