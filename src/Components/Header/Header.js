import React from 'react';
import './Header.css';

const Header = () => (
	<div className="header">
		<ul>
			<li>
				<a href="/">Home</a>
			</li>
			<li>
				<a href="/search-joke">Search</a>
			</li>
			<li>
				<a href="/never-ending-list">Never Ending List</a>
			</li>
		</ul>
	</div>
);

export default Header;
