import React from 'react';

import Button from '../../../Styled Components/Button';
import Input from '../../../Styled Components/Input';

class SearchJoke extends React.Component {
	state = {
		searchTermFirst: '',
		searchTermLast: '',
		searchResults: []
	};

	handleChangeFirst = (e) => {
		this.setState({
			searchTermFirst: e.target.value
		});
	};

	handleChangeLast = (e) => {
		this.setState({
			searchTermLast: e.target.value
		});
	};

	handleSubmit = (event) => {
		event.preventDefault();
		this.getJoke();
	};

	async getJoke() {
		const request = await fetch(
			`http://api.icndb.com/jokes/random?firstName=${this.state.searchTermFirst}&lastName=${this.state
				.searchTermLast}`
		);
		const response = await request.json();
		this.setState({
			searchResults: Object.values(response.value)
		});
		console.log(this.state.searchResults[1]);
	}
	render() {
		const { searchTermFirst, searchTermLast, searchResults } = this.state;
		return (
			<div>
				<div>
					<p>Enter a first and last name to get a personalised joke, the Chuck Norris way!</p>
				</div>
				<form>
					<Input
						onChange={this.handleChangeFirst}
						placeholder="First Name"
						type="text"
						name="searchTermFirst"
						value={searchTermFirst}
					/>
					<Input
						onChange={this.handleChangeLast}
						placeholder="Last Name"
						type="text"
						name="searchTermLast"
						value={searchTermLast}
					/>
					<Button primary onClick={this.handleSubmit} className="btn btn-success">
						Random Joke
					</Button>
				</form>
				<div>
					<p>{searchResults[1]}</p>
				</div>
			</div>
		);
	}
}

export default SearchJoke;
