import React from 'react';

import Button from '../../../Styled Components/Button';

class SingleJoke extends React.Component {
	state = {
		randomJoke: [],
		show: false
	};

	async getSingleJoke() {
		const request = await fetch('http://api.icndb.com/jokes/random');
		const response = await request.json();
		this.setState({
			randomJoke: Object.values(response.value)
		});
		console.log(this.state.randomJoke[1]);
	}

	toggle = () => {
		const { show } = this.state;
		this.setState({
			show: !show
		});
	};

	handleSubmit = (event) => {
		event.preventDefault();
		this.getSingleJoke();
	};

	render() {
		return (
			<div>
				<p>Click this button to generate a random Chuck Norris joke</p>
				<Button primary onClick={this.handleSubmit}>Random Joke</Button>
				<div>
					<p>{this.state.randomJoke[1]}</p>
					</div>
			</div>
		);
	}
}

export default SingleJoke;
