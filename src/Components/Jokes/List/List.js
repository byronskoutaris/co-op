import React from 'react';
import fetch from 'isomorphic-fetch';

import './List.css';

class List extends React.Component {
	state = {
		loading: false,
		list: []
	};

	async loadData() {
		const request = await fetch('http://api.icndb.com/jokes/random/175');
		const response = await request.json();
		let re = response.value.map(({ joke }) => joke);
		this.setState({
			list: [ ...this.state.list, ...re ]
		});
		// console.log(this.state.list)
		// console.log(re)
	}

	handleScroll = () => {
		var lastLi = document.querySelector('ul.list > li:last-child');
		var lastLiOffset = lastLi.offsetTop + lastLi.clientHeight;
		var pageOffset = window.pageYOffset + window.innerHeight;
		if (pageOffset > lastLiOffset) {
			this.loadData();
		}
	};

	handleSubmit = (e) => {
		e.preventDefault();
		this.loadData();
	};

	UNSAFE_componentWillMount() {
		this.loadData();
		this.scrollListener = window.addEventListener('scroll', (e) => {
			this.handleScroll(e);
		});
	}

	render() {
		return (
			<div>
				<div>
					<p>If you are sure what you are doing, scroll down for an unlimited Chuck Norris jokes list</p>
				</div>
				<ul className="list">
					<li>{this.state.list}</li>
				</ul>
				<button onClick={this.handleSubmit}>Load More</button>
			</div>
		);
	}
}

export default List;
