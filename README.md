
## Get Started


### `npm install`

### `npm start`

## Features implemented:

- Click button to get a single random joke from the API

- Input a first and last name to generate a custom Chuck Norris joke from the API

- Never-Ending list of jokes

- Basic styles written in CSS and Styled Components